package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getBookPrice(String isbn) {
        HashMap<String, Double> hashMap = new HashMap<>();

        hashMap.put("0", 0.0);
        hashMap.put("1", 10.0);
        hashMap.put("2", 45.0);
        hashMap.put("3", 20.0);
        hashMap.put("4", 35.0);
        hashMap.put("5", 50.0);

        if (Integer.parseInt(isbn) > 0 && Integer.parseInt(isbn) < 6) {
            return hashMap.get(isbn);
        } else {
            return 0;
        }
    }
}
